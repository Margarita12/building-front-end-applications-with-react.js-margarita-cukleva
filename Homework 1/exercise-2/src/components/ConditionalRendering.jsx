import React, { useState } from "react";

function ConditionalRendering() {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <div>
      {isVisible ? (
        <div>
          <p>Welcome, User!</p>
          <button
            onClick={() => {
              setIsVisible(!isVisible);
            }}
          >
            Log out
          </button>
        </div>
      ) : (
        <button
          onClick={() => {
            setIsVisible(!isVisible);
          }}
        >
          Login
        </button>
      )}
    </div>
  );
}

export default ConditionalRendering;
