import React, { useState } from "react";

function ColorPicker() {
  const [color, setColor] = useState("");

  return (
    <div>
      <p>Selected Color {color}</p>
      <button onClick={() => setColor("red")}>Red</button>
      <button onClick={() => setColor("blue")}>Blue</button>
      <button onClick={() => setColor("green")}>Green</button>
    </div>
  );
}

export default ColorPicker;
