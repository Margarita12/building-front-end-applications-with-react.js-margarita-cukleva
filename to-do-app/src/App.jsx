import React, {useState} from 'react';
import Header from './components/Header/Header';
import Tasks from './components/Tasks/Tasks';
import './App.css';

// kluc za localStorage
const LOCAL_STORAGE_KEY = "";

function App() {

  const [tasks, setTasks] = useState([])

  function setTaskAndSave(newTasks) {
    // se povikuva setTasks koja pravi update na taskovite, so niza newTasks
    setTasks(newTasks)
    // localStorage.setItem ovoj metod pravi update na site taskovi koi se dodadeni vo local storage i ima kluc koj e LOCAL_STORAGE_KEY

    localStorage.setItem(LOCAL_STORAGE_KEY, newTasks)
  }

  function addTask(taskTitle) {
    setTaskAndSave([
      ...tasks,
      {
        // so crypto.randomUUID() dava random id koj e bezbeden (secure random number generator)
        id: crypto.randomUUID(),
        title: taskTitle,
        isCompleted: false
      }
    ])
  }

// f-ja koja ovozmozuva toggle efekt koga ke se klikne na radio button da se shkrtne vrednosta i obratno
  function toggleTaskCompletedById(taskId) {
    const newTasks = tasks.map(task => {
      if (task.id === taskId) {
        return { ...task, isCompleted: !task.isCompleted }
      }
      return task
    })
    // setTaskAndSave(newTasks)
    setTasks(newTasks)
  }

  // f-ja koja brise task
  function deleteTaskById(taskId) {
    const newTasks = tasks.filter(task => task.id != taskId);
    setTasks(newTasks)

  }



  return (
    <>
      <Header onAddTask={addTask} />
      <Tasks
        tasks={tasks}
        onComplete={toggleTaskCompletedById}
        onDelete={deleteTaskById}
      />
    </>
  )
}

export default App;
