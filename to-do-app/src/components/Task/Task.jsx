import React from "react";
import styles from "../Task/Task.module.css";
import { AiFillDelete, AiFillCheckCircle } from "react-icons/ai";


function Task({ task, onComplete, onDelete }){
    return(

        <div className={styles.task}>
            
            {/* radio button */}
            <button className={styles.checkContainer} onClick={() => onComplete(task.id)}>
                {/* ako e zavrsen taskot da se klikne na kopceto ako ne, da ne se pojavuva nisto t.e prazen div  */}
                {task.isCompleted ? <AiFillCheckCircle /> : < div />}
            </button>

            {/* paragraf za imeto na taskot so ke bide dodaden */}
            {/* ako e zavrsen taskot t.e kliknato e radio buttonot togas da se pojavi stilot na klasata taskCompleted(da se shkrtne) ako ne da si stoi samo tekstot */}
            <p className={task.isCompleted ? styles.taskCompleted : ""}>{task.title}</p>

            {/* kopceto za delete - na onClick treba da ja povika onDelete i da go zeme id-to na taskot so sakam da go izbrisam */}
            {/*  AiFillDelete e ikonata korpa za brisenje so golemina 28*/}
            <button className={styles.delBtn} onClick={() => onDelete(task.id)}>
                <AiFillDelete size={28} />
            </button>
        </div>

    )
}

export default Task;