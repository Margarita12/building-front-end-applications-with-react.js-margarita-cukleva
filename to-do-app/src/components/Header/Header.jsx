import React, {useState} from "react";
import styles from "../Header/Header.module.css";

function Header({onAddTask}){

        const [title, setTitle] = useState('')

    // f-ja za da moze da se klikne na create kopceto i so toa da se dodade taskot koj ni e napisan vo poleto
    //t.e da moze submit na formata, na klik na kopce da moze da se izvrsi nekoja funkcionalnost
    function handleSubmit(event) {
        event.preventDefault()
        if (title == '') {
            return null;
        } else {
            onAddTask(title)
            setTitle('')
        }
    }

    // f-ja za da moze da se pisuva vo input poleto
    function onChangeTitle(event) {
        setTitle(event.target.value)
    }

    return (
        <header className={styles.header}>

            <form  onSubmit={handleSubmit} className={styles.newTaskForm}>
                <input type="text" placeholder='Add a New Task' value={title} onChange={onChangeTitle}/>
                <button>
                    Create
                </button>
            </form>
        </header>
    )

}


export default Header;