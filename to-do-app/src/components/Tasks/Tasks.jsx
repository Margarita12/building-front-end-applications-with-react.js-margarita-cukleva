import React from "react";
import styles from "../Tasks/Tasks.module.css";
import Task from "../Task/Task";

function Tasks({ tasks, onComplete, onDelete }){
    return(
        <section className={styles.tasks}>
          

                {/* vo ovoj div se stavaat site taskovi so se dodadeni preku create kopceto kako vo edna lista kade so map se mapiraat */}
            <div className={styles.list}>
                {tasks.map(task => (
                    // gi ima task,onComplete,onDelete bidejki istite tie gi ima kako atributi i vo Task komponentata
                    <Task key={task.id} task={task} onComplete={onComplete} onDelete={onDelete} />
                ))}
            </div>
        </section>

    )
}

export default Tasks;